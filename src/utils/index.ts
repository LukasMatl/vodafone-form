import {join} from "ramda";

export const formatPhoneNumber = (phoneNumber: string = ''): string =>
    join(' ')(phoneNumber.split(/(\d{3})/)
    .filter(Boolean)
    .filter((val: string, index: number) => val && index < 3));

export const deformatPhoneNumber = (phoneNumber: string = ''): string =>
    phoneNumber.replace(/\D/g,'');;


export const getRandomElement = (array: Array<any> = []) =>
    array[Math.floor(Math.random()*array.length)];