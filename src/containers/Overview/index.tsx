import React, {Component} from "react";
import {OverviewProps} from "../../interfaces/containers";
import {withStyles} from "@material-ui/core";
import PersonalDetailsSection from "./PersonalDetailsSection";
import ServiceSection from "./ServiceSection";
import NotesSection from "./NotesSection";
import ParametersSection from "./ParametersSection";

class Overview extends Component<OverviewProps> {

  render() {
    const {classes} = this.props;

    return (
        <div className="overview">
          <h1>Zalozit novy lead</h1>
          <PersonalDetailsSection/>
          <ServiceSection/>
          <NotesSection/>
          <ParametersSection/>
        </div>
    );
  }
}


export default withStyles({})(Overview);