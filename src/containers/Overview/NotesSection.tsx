import React, {Component} from "react";
import {SectionProps} from "../../interfaces/containers";
import {Grid, TextField, withStyles} from "@material-ui/core";
import {applySpec} from "ramda";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {modifyFormData} from "../../actions/formActions";
import {getFormData} from "../../selectors";

class NotesSection extends Component<SectionProps> {

  constructor(props: any) {
    super(props);
  }

  render() {
    const { formData, modifyFormData } = this.props;

    return (
        <section className="note-section">
          <h3>3. Poznámky</h3>
          <Grid container spacing={8}>
            <Grid item xs={12}>
              <TextField
                  label="Poznámka"
                  placeholder="Poznámka"
                  multiline
                  fullWidth
                  rows={4}
                  rowsMax={50}
                  margin="normal"
                  variant="filled"
                  value={formData['note']}
                  onChange={(event) => modifyFormData('note', event.target.value)}
              />
            </Grid>
          </Grid>
        </section>
    );
  }
}

const mapStateToProps = applySpec({
  formData: getFormData
});

const mapDispatchToProps = (dispatch: any) => ({
  ...bindActionCreators(
      {
        modifyFormData
      },
      dispatch
  ),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles({})(NotesSection));

