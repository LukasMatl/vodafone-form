import React, {Component} from "react";
import {SectionProps} from "../../interfaces/containers";
import {Grid, MenuItem, TextField, withStyles} from "@material-ui/core";
import {applySpec} from "ramda";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import { modifyFormData, sendForm} from "../../actions/formActions";
import {getFormData} from "../../selectors";
import {ASSIGN_OPTIONS} from "../../constants";

class ParametersSection extends Component<SectionProps> {

  constructor(props: any) {
    super(props);
  }

  render() {
    const { formData, modifyFormData, sendForm } = this.props;

    return (
        <section className="parameters-section">
          <h3>4. Parametry leadu</h3>
          <Grid container spacing={8}>
            <Grid item xs={4}>
              <TextField
                  id="datetime-local"
                  label="Termín kontaktu"
                  type="datetime-local"
                  value={formData['scheduledAppointment']}
                  onChange={(event) => modifyFormData('scheduledAppointment', event.target.value)}
                  variant="filled"
                  fullWidth
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                  select
                  value={formData['assigned']}
                  label="Přiřazení"
                  onChange={(event) => modifyFormData('assigned', event.target.value)}
                  variant="filled"
                  fullWidth
              >
                {ASSIGN_OPTIONS.map(option => (
                    <MenuItem key={option} value={option}>
                      {option}
                    </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={4}>
              <button className="btn btn-submit" onClick={() => sendForm()}>
                Uložit
              </button>
            </Grid>
          </Grid>
        </section>
    );
  }
}

const mapStateToProps = applySpec({
  formData: getFormData
});

const mapDispatchToProps = (dispatch: any) => ({
  ...bindActionCreators(
      {
        modifyFormData,
        sendForm
      },
      dispatch
  ),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles({})(ParametersSection));

