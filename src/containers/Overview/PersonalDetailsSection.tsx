import React, {Component} from "react";
import {SectionProps} from "../../interfaces/containers";
import {Grid, Paper, TextField, withStyles} from "@material-ui/core";
import {map, applySpec, ifElse, has} from "ramda";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {modifyFormData} from "../../actions/formActions";
import {getFormData} from "../../selectors";
import {deformatPhoneNumber, formatPhoneNumber} from "../../utils";

class PersonalDetailsSection extends Component<SectionProps> {

  constructor(props: any) {
    super(props);
  }

  fields:any = [];

  fieldsDef = () => [
    {name: 'firstName', label: 'Jméno'},
    {name: 'lastName', label: 'Přijmení'},
    {name: 'phoneNumber', label: 'Telefon', mask: true}
  ];

  createFields = (fieldDefinition: any) => {
    const { formData, modifyFormData } = this.props;
    // @ts-ignore
    const field = ifElse(has('mask'),
        () => (
            <TextField
                fullWidth
                id={fieldDefinition.name}
                label={fieldDefinition.label}
                placeholder={fieldDefinition.label}
                margin="dense"
                variant="filled"
                value={formatPhoneNumber(formData[fieldDefinition.name])}
                onChange={(event) => modifyFormData(fieldDefinition.name, deformatPhoneNumber(event.target.value))}
            />
        ),
        () => (
            <TextField
                fullWidth
                id={fieldDefinition.name}
                label={fieldDefinition.label}
                placeholder={fieldDefinition.label}
                margin="dense"
                variant="filled"
                value={formData[fieldDefinition.name]}
                onChange={(event) => modifyFormData(fieldDefinition.name, event.target.value)}
            />
        )
        )(fieldDefinition);
    return (
        <Grid key={fieldDefinition.name} item xs={4}>
          {field}
        </Grid>
    )
  };

  render() {
    return (
        <section className="personal-section">
          <h3>1. Zákazník</h3>
          <Grid container spacing={8}>
            {map((fieldDefinition: any) =>
                this.createFields(fieldDefinition), this.fieldsDef())}
          </Grid>
        </section>
    );
  }
}

const mapStateToProps = applySpec({
  formData: getFormData
});

const mapDispatchToProps = (dispatch: any) => ({
  ...bindActionCreators(
      {
        modifyFormData
      },
      dispatch
  ),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles({})(PersonalDetailsSection));

