import React, {Component} from "react";
import {SectionProps} from "../../interfaces/containers";
import {Checkbox, Grid, Paper, TextField, withStyles} from "@material-ui/core";
import {map, applySpec, ifElse, has, prop} from "ramda";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {modifyFormData} from "../../actions/formActions";
import {getFormData} from "../../selectors";
import {deformatPhoneNumber, formatPhoneNumber} from "../../utils";

class ServiceSection extends Component<SectionProps> {

  constructor(props: any) {
    super(props);
  }

  fieldsDef = () => [
    {name: 'isCallActive', label: 'Volání', icon: 'fas fa-phone'},
    {name: 'isInternetActive', label: 'Internet', icon: 'fas fa-house-damage', available: 'isInternetAvailable'},
    {name: 'isDataSimActive', label: 'Data SIM', icon: 'fas fa-sim-card'},
    {name: 'isTVActive', label: 'TV', icon: 'fas fa-tv', available: 'isTVAvailable'}

  ];

  createFields = (fieldDefinition: any) => {
    const { formData, modifyFormData } = this.props;
    const checked = formData[fieldDefinition.name];
    const field = (<div className={`service-field ${checked && 'service-field--active'}`}
                        onClick={() => modifyFormData(fieldDefinition.name, !checked)}>
      <Checkbox checked={checked} color="primary"/>
      <label>{fieldDefinition.label}</label>
      <i className={fieldDefinition.icon}/>
    </div>);

    return !has('available', fieldDefinition) || prop(fieldDefinition.available, formData)? (
        <Grid key={fieldDefinition.name} item xs={4}>
          {field}
        </Grid>
    ): null
  };

  addressField = () => {
    const { formData, modifyFormData } = this.props;

    return formData['isInternetActive'] && (
        <Grid item xs={6}>
          <TextField
              fullWidth
              label="Adresa"
              placeholder="Vyplňte adresu"
              margin="dense"
              variant="filled"
              value={formData['address']}
              onChange={(event) => modifyFormData('address', event.target.value)}
          />
        </Grid>
    )
  };

  unavailableServices = () => {
    const { formData } = this.props;
    const isInternetAvailable = formData['isInternetAvailable'];
    const isTVAvailable = formData['isTVAvailable'];
    const internetSpeed = formData['internetSpeed'];
    return (
        <React.Fragment>

          {isInternetAvailable && <Grid item xs={3}>
            <div className="internet-available">
              <i className="fas fa-check"/>
              <label>Dostupných až {internetSpeed} Mbps</label>
            </div>
          </Grid>}
          {!isTVAvailable && <Grid item xs={3}>
            <div className="service-unavailable">
              <i className="fas fa-times"/>
              <label>Televize je nedostupná</label>
            </div>
          </Grid>}
          {!isInternetAvailable && <Grid item xs={3}>
            <div className="service-unavailable">
              <i className="fas fa-times"/>
              <label>Internet je nedostupný</label>
            </div>
          </Grid>}
        </React.Fragment>
    )
  };

  render() {
    return (
        <section className="service-section">
          <h3>2. Poptává služby</h3>
          <Grid container spacing={8}>
            {map((fieldDefinition: any) =>
                this.createFields(fieldDefinition), this.fieldsDef())}
            {this.addressField()}
            {this.unavailableServices()}
          </Grid>
        </section>
    );
  }
}

const mapStateToProps = applySpec({
  formData: getFormData
});

const mapDispatchToProps = (dispatch: any) => ({
  ...bindActionCreators(
      {
        modifyFormData
      },
      dispatch
  ),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles({})(ServiceSection));

