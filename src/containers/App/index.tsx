import React, { Component } from 'react';
import Header from '../../components/Header';
import SubHeader from "../../components/SubHeader";


class App extends Component {

  componentDidMount = () => {
    require('../../scss/app.scss');
  }

  render() {
    const {children} = this.props;
    return (
          <div className="app">
            <Header/>
            <SubHeader/>
            <section className="content">
              {children}
            </section>
          </div>
    );
  }
}

export default App;
