import React, {Component} from "react";
import {Provider} from "react-redux";
import App from "../App/index";
import {HashRouter, Switch} from "react-router-dom";
import {RootProps} from "../../interfaces/containers";

class Root extends Component<RootProps> {

  render() {
    const {store,  routes} = this.props;
    return (
        <Provider store={store}>
          <App>
            <HashRouter>
              <Switch>
                {routes}
              </Switch>

              </HashRouter>
          </App>
        </Provider>
    );
  }
}


export default Root;