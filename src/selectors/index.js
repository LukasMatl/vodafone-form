import {createSelector} from 'reselect';
import {
  identity, prop, path
} from 'ramda';

// api
export const getApi = createSelector(path(['api']), identity);
export const getLoadingState = createSelector([getApi], prop('loading'));

// formData
export const getFormData = createSelector(path(['formData']), identity);

