import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import formDataReducer from "./formReducer";
import apiReducer from "./apiReducer";


export const rootReducer = combineReducers({
  routing: routerReducer,
  formData: formDataReducer,
  api: apiReducer
});