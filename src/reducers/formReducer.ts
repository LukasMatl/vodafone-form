// @ts-ignore
import {} from "ramda";
import {actionTypes, ASSIGN_OPTIONS, INTERNET_SPEED_OPTIONS, TRUE_OR_FALSE_OPTIONS} from "../constants";
// @ts-ignore
import {mergeRight} from "ramda";
import {getRandomElement} from "../utils";

const isInternetAvailable = getRandomElement(TRUE_OR_FALSE_OPTIONS);
const isTVAvailable = !isInternetAvailable;
const internetSpeed = isInternetAvailable? getRandomElement(INTERNET_SPEED_OPTIONS): 0;

const initState = {
  firstName: '',
  lastName: '',
  phoneNumber: '',
  isCallActive: false,
  isInternetActive: false,
  isDataSimActive: false,
  isTVActive: false,
  address: '',
  note: '',
  assigned: ASSIGN_OPTIONS[0],
  scheduledAppointment: '',
  isTVAvailable,
  isInternetAvailable,
  internetSpeed
};

const dataReducer = (state: any = initState, action: any) => {
  switch (action.type) {
    case actionTypes.FORM_MODIFICATION:
      return mergeRight(state, action.payload);
    default:
      return state;
  }
}

export default dataReducer;