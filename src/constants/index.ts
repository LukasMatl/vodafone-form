import {F, T} from "ramda";

export  const API_URL = 'http://localhost:8080/someEndpoint';


export const actionTypes: any = {
  API_LOADING: 'API/LOADING',
  API_STOP_LOADING: 'API/STOP_LOADING',
  API_STOP_LOADING_WITH_ERROR: 'API/STOP_LOADING_WITH_ERROR',
  FORM_MODIFICATION: 'FORM/MODIFICATION',
}

export const TRUE_OR_FALSE_OPTIONS = [true, false];
export const INTERNET_SPEED_OPTIONS = [10,50,100,150,666];


export const ASSIGN_OPTIONS = [
    'Já', 'Callcentrum', 'Prodejna'
]