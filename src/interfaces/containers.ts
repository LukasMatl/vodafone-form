import {modifyFormData, sendForm} from "../actions/formActions";

export interface RootProps  {
  store: any,
  routes: Array<any>
}

export interface OverviewProps  {
  classes: any
}

export interface SectionProps {
  formData: any,
  modifyFormData: Function
  sendForm: Function
}