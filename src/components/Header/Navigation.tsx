import React from "react";

const Navigation = () => (
    <nav>
      <ul>
        <li><a href="#"><i className="icon-home"></i>Přehled</a></li>
        <li><a href="#statistics"><i className="icon-user"></i>Statistiky</a></li>
      </ul>
    </nav>
)


export default Navigation;