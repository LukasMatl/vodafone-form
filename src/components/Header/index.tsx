import React from 'react';
import logo from '../../assets/img/logo.png';
import Navigation from './Navigation';
import MenuIcons from "./MenuIcons";
import Avatar from "./Avatar";


const Header = () => (
    <header className="header">
      <section>
        <a href="#"><img className="nav-logo" src={logo} /></a>
        <Navigation/>
        <MenuIcons/>
        <Avatar/>
      </section>
    </header>
)


export default Header;