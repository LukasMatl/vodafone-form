import React from "react";

const MenuIcons = () => (
    <div className="menu-icons">
      <i className="fas fa-home"/>
      <i className="fas fa-bell"/>
      <label htmlFor="toggle-nav" className="toggle-menu">
        <ul>
          <li/>
          <li/>
          <li/>
        </ul>
      </label>
      <input type="checkbox" id="toggle-nav"/>
    </div>
);

export default MenuIcons;


