import {createAction} from "redux-actions";
import {actionTypes} from "../constants/index";

export const loading = () => (dispatch: Function) => {
  dispatch(createAction(actionTypes.API_LOADING)());
}

export const stopLoading = () => (dispatch: Function) => {
  dispatch(createAction(actionTypes.API_STOP_LOADING)());
}

export const stopLoadingWithError = () => (dispatch: Function) => {
  dispatch(createAction(actionTypes.API_STOP_LOADING_WITH_ERROR)());
}