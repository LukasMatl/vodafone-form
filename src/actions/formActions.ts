import axios from 'axios';
import {actionTypes, API_URL} from "../constants";
import {loading, stopLoading, stopLoadingWithError} from "./apiActions";
import {divide, pipe, sum, map} from "ramda";
import {createAction} from "redux-actions";
import {getFormData} from '../selectors';

const postForm = () => (dispatch: Function, getState: Function) => {
  const formData = getFormData(getState());
  console.log(formData);
  axios({
    method: 'POST',
    url: '/someEndpoint',
    data: formData
  }).then( (response) => {
    console.log(response);
  }).catch( (error) => {
    console.log(error);
  });
}


export const sendForm = ()  => (dispatch: Function) => {
    Promise.all([
        dispatch(loading()),
        dispatch(postForm())
    ]).then(() => {
      dispatch(stopLoading());
    }).catch(() => dispatch(stopLoadingWithError()))
}

export const modifyFormData = (fieldName: string, value: any) =>
    (dispatch: Function) => dispatch(createAction(actionTypes.FORM_MODIFICATION)({[fieldName]: value}))




