import React from 'react';
import {Route} from 'react-router';
import Overview from './containers/Overview';
import Statistics from "./containers/Statistics";

const routes = [
  <Route key='1' exact path={'/'} component={Overview}/>,
  <Route key='2' exact path={'/statistics'} component={Statistics}/>,
];

export default routes;
